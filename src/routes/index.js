import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '@/views/Home.vue';
import ErrorPage from '@/views/Error.vue';
import TutorialPage from '@/views/Tuto.vue';
Vue.use(Router)

export default new Router({
    routes: [
      // You should use named routes, best practices
      { path: '', component: HomePage, name: 'Home'},
      { path: '/tuto', component: TutorialPage, name: 'Tutorial'},
      // Error Page
      { path: '*', component: ErrorPage, name: 'Not-Found'},

    ]
})
