import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
// How to use Vue Router
import router from '@/routes/index'

Vue.config.productionTip = false

new Vue({
  // Add the router import in the Vue Launcher
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
