---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://positivethinking.tech/wp-content/uploads/2021/01/Logo-Vuejs.png)

# **VueJs Tutoriel**

Framework Javascript Front End

https://vuejs.org/

---

# Installation 

Pour utiliser VueJs, il faut tout d'abord installer le CLI de Vue
(https://cli.vuejs.org/guide/installation)

<br/>

```sh
$ npm install -g @vue/cli
# OU
$ yarn global add @vue/cli
```

---

# Création de projet

<br />

Créer un projet avec vue from scratch
```sh
$ vue create <app-name>
```

---

Ou depuis un projet html existant :

index.html
```html
<script src="https://unpkg.com/vue@3"></script>
<div id="app">{{ message }}</div>

<script>
  const { createApp } = Vue

  createApp({
    data() {
      return {
        message: 'Hello Vue!'
      }
    }
  }).mount('#app')
</script>
```
---
# Utiliser l'interface de creation

VueJs intègre une interface pour créer des projets VueJs et les gérer. pour lancer cette interface :

```sh
$ vue ui
```

---

# Fonctionnement de VueJs

Se compose de composants : **_"Tout est composant"_**
#### Architecture du projet (1/2): 
```sh
- babel.config.js
- jsconfig.jscon 
- node_modules # dossier des dépendances binaires installées
- package-lock.json
- package.json #fichier de dépendances principales
- public # dossier de rendu du projet
- readme.md
- src/ #Dossier principal pour le développement
- vue.config.js
```

---

#### Contenu du dossier ```src/``` :

<br />

```sh
- main.js # point d'entrée de l'application JS
- components/ # dossier des composants vue (.vue)
- App.vue # Composant principal
- assets/ # dossier des statics (images et logos)
```

---

# Architecture d'un composant VueJs

```js
<template>
  <div id="app">
    <img alt="Vue logo" src="./assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
  </div>
</template>

<script>
import HelloWorld from './components/HelloWorld.vue'

export default {
  name: 'App',
  components: {
    HelloWorld
  }
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>



```
---